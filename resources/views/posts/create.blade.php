@extends('layouts.app')

@section('content')
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


  <h2>Create Post</h2>
  {!! Form::open(['action' => 'PostsController@store', 'method' => 'POST']) !!}
    <div class="form-group">
      {{FORM::label('name','Name')}}
      {{FORM::text('name','', ['class' => 'form-control','placeholder' => 'Title'])}}
    </div>
    <div class="form-group">
      {{FORM::label('body','Body')}}
      {{FORM::textarea('body','', ['id'=>'article-ckeditor', 'class' => 'form-control','placeholder' => 'Body Text'])}}
      {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
    </div>
    {!! Form::close() !!}



@endsection
