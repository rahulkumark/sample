@extends('layouts.app')

@section('content')
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <h2>POSTS</h2>
  @if(count($posts) > 0 )
    @foreach ($posts as $post)
      <div class="well">
        <h3> <a href="/posts/{{$post->id}}">{{ $post->name }}</a></h3>
        <small>written on {{ $post->created_at }}  </small>
      </div>
    @endforeach
    {{ $posts->links()}}
    @else
      <p>No Posts Found</p>
  @endif
@endsection
