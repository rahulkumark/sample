@extends('layouts.app')

@section('content')
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


  <h2>Edit Post</h2>
  {!! Form::open(['action' => ['PostsController@update',$post->id] , 'method' => 'POST']) !!}
    <div class="form-group">
      {{FORM::label('name','Name')}}
      {{FORM::text('name',$post->name, ['class' => 'form-control','placeholder' => 'Title'])}}
    </div>
    <div class="form-group">
      {{FORM::label('body','Body')}}
      {{FORM::textarea('body',$post->body, ['id'=>'article-ckeditor', 'class' => 'form-control','placeholder' => 'Body Text'])}}
    </div>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
  {!! Form::close() !!}

@endsection
