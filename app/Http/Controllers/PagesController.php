<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    { $dpara = 'Index coming from a dynamic value' ;
      //return view('pages.index', compact('iname'));
      return view('pages.index')->with('dpara', $dpara);
    }

    public function about()
    {
      return view('pages.about');
    }

    public function services()
    { $someArray = array (
       'title' => 'Services',
       'services' => [ 'Web Design', 'BackEnd', 'FrontEnd' ]
     );

      return view('pages.services')->with($someArray);
    }
}
